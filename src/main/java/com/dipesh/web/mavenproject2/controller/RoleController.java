/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.web.mavenproject2.controller;

import com.dipesh.web.mavenproject2.entity.Role;
import com.dipesh.web.mavenproject2.repository.RoleRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping(value = "/role")
public class RoleController {
    @Autowired
    private RoleRepository roleRepository;
    
    @GetMapping
    @ResponseBody
    public List<Role> index(){
        return roleRepository.findAll();
    }
}
