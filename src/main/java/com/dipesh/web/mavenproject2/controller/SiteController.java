/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.web.mavenproject2.controller;

import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author Dipesh
 */
public abstract class SiteController {
    protected String pageTitle="Management System";
    protected String pageURI;
    protected String viewPath;
    
    @ModelAttribute(value = "pageTitle")
    public String getpagetitle(){
        return pageTitle;
    }
    @ModelAttribute(value="pageURI")
    public String getpagrURI(){
        return pageURI; 
    }
    @ModelAttribute(value="viewPath")
    public String getviewpath(){
        return viewPath; 
    }
}
