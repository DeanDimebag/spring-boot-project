 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.web.mavenproject2.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Dipesh
 */
@Controller
public abstract class CRUDController<T, ID> extends SiteController{
    
    @Autowired
    public JpaRepository<T, ID> respository;
    
    
    //for fetching
    @GetMapping
    public String index(Model model,@RequestParam(value = "order",required = false)String order){
        Sort sort=Sort.by(Sort.Direction.DESC,"id");
        if(order!=null && !order.isEmpty()){
            sort=Sort.by(Sort.Direction.DESC,order);
        }
        model.addAttribute("records", respository.findAll(sort));
        return viewPath+"/index";   
    }
    
    //for create
    @GetMapping (value = "/create")
    public String create(Model model){
        return viewPath +"/create";
    }
    
    //for edit
    @GetMapping( value = "/edit/{id}")
    public String edit(@PathVariable("id")ID id, Model model){
        model.addAttribute("record", respository.findById(id).get());
        return viewPath+"/edit";
    }
    
    //for posting data
    @PostMapping
    public String save(T model){
        respository.save(model);
        return "redirect:/"+viewPath+"?success";
    }
    
    //for delete
    @GetMapping (value = "/delete/{id}")
    public String delete(@PathVariable("id")ID id, Model model){
        respository.deleteById(id);
        return "redirect:/"+pageURI+"?success";
    }
    
    @GetMapping(value = "/json")
    @ResponseBody
    public List<T> json(){
        return respository.findAll();
    }
    
}
