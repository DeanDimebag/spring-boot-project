/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.web.mavenproject2.controller;

import com.dipesh.web.mavenproject2.entity.Customer;
import com.dipesh.web.mavenproject2.repository.CompanyRespository;
import com.dipesh.web.mavenproject2.repository.CustomerRepository;
import com.dipesh.web.mavenproject2.repository.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**

 * @author Dipesh
 */
@Controller
@RequestMapping (value = "/customer")
public class CustomerController extends CRUDController<Customer, Integer>{
    
    
    @Autowired
    private PositionRepository positionRepository;
    
    @Autowired
    private CompanyRespository companyRespository;
    
    @Autowired
    private CustomerRepository customerRepository;
    
    public CustomerController() {
        viewPath="customer";
        pageTitle="Customer";
        pageURI="customer";
    }
    
    @GetMapping(value = "/active")
    public String active(Model model){
        model.addAttribute("records", customerRepository.myactiveCustomer());
        return viewPath+"/index";   
    }
    
    @GetMapping(value = "/inactive")
    public String inactive(Model model){
        model.addAttribute("records", customerRepository.myinactiveCustomer());
        return viewPath+"/index";   
    }

    @Override
    public String create(Model model) {
        model.addAttribute("companies", companyRespository.findAll());
        model.addAttribute("positions", positionRepository.findAll());
        return super.create(model); 
    }

    @Override
    public String edit(@PathVariable("id")Integer id, Model model) {
        model.addAttribute("companies", companyRespository.findAll());
        model.addAttribute("positions", positionRepository.findAll());
        return super.edit(id, model);
    }
    
    @GetMapping(value = "/detail/{id}")
    public String detail(@PathVariable("id")Integer id, Model model){
        model.addAttribute("records", customerRepository.findById(id).get());
        return viewPath+"/detail";
    }
}
