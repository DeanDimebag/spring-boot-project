/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.web.mavenproject2.controller;

import com.dipesh.web.mavenproject2.entity.User;
import com.dipesh.web.mavenproject2.repository.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping (value = "/user")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    
    @GetMapping
    public List<User> index(){
        return userRepository.findAll();
    }
    @PostMapping
    public String save(User user){
        userRepository.save(user);
        return "redirect:/login";
    }
}
