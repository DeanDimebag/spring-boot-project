/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.web.mavenproject2.controller;

import com.dipesh.web.mavenproject2.entity.User;
import com.dipesh.web.mavenproject2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping(value = "/register")
public class RegisterController {
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @GetMapping
    public String register(){
        return "/register/index";
    }
    
//    @PostMapping
//    public String save(User user){
//        user.setUsername(userRepository.findByUsername(username));
//        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
//        user.setEmail(userRepository.);
//        userRepository.save(user);
//        return "redirect:/login?success";
//    }
    @PostMapping
    public String save(User user){
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return "redirect:/login?success";
    }
}
