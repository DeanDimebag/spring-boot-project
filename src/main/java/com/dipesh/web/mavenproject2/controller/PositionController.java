/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.web.mavenproject2.controller;

import com.dipesh.web.mavenproject2.entity.Position;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping(value = "/position")
public class PositionController extends CRUDController<Position, Integer> {

    public PositionController() {
        pageTitle = "Position";
        pageURI = "position";
        viewPath = "position";
    }

}
