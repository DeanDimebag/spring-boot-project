/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.web.mavenproject2.controller;

import com.dipesh.web.mavenproject2.entity.Company;
import com.dipesh.web.mavenproject2.repository.CompanyRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping(value = "/company")
public class CompanyController extends CRUDController<Company, Integer>{
    
    @Autowired
    private CompanyRespository companyRespository;
    
    public CompanyController() {
        viewPath="company";
        pageTitle="Company";
        pageURI="company";
    }
    
    @GetMapping(value = "/active")
    public String active(Model model){
        model.addAttribute("records", companyRespository.myactiveCompany());
        return viewPath+"/index";   
    }
    
    @GetMapping(value = "/inactive")
    public String inactive(Model model){
        model.addAttribute("records", companyRespository.myinactiveCompany());
        return viewPath+"/index";   
    }
    
    @GetMapping (value = "/detail/{id}")
    public String detail(@PathVariable("id") Integer id,Model model){
        model.addAttribute("records", companyRespository.findById(id).get());
        return viewPath+"/detail";
    }
}
