/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.web.mavenproject2.service;

import com.dipesh.web.mavenproject2.entity.User;
import com.dipesh.web.mavenproject2.entity.UserRole;
import com.dipesh.web.mavenproject2.repository.UserRepository;
import com.dipesh.web.mavenproject2.repository.UserRoleRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Dipesh
 */
@Service
public class AuthUserServices implements UserDetailsService{

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user==null){
            throw new UsernameNotFoundException("User donot exist..");
        }
        UserDetails userDetails =new org.springframework.security.core.userdetails
                .User(username, user.getPassword(), getAuthorities(username));
        return userDetails;
    }
    private List<GrantedAuthority> getAuthorities(String username){
        List<GrantedAuthority> authorities = 
                new ArrayList<>();
        for(UserRole role:userRoleRepository.findByUserIdUsername(username)){
            authorities.add(new SimpleGrantedAuthority(role.getRoleId().getRoleName()));
        }
        
        return authorities;
    }
}
