/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.web.mavenproject2.repository;

import com.dipesh.web.mavenproject2.entity.Company;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Dipesh
 */
public interface CompanyRespository extends JpaRepository<Company, Integer> {
    @Query(value = "select * from tbl_company where status=1", nativeQuery = true)
    List<Company> myactiveCompany();
    
    @Query(value = "select * from tbl_company where status=0", nativeQuery = true)
    List<Company> myinactiveCompany();
    
    @Query(value = "select * from tbl_company where name=?", nativeQuery = true)
   List<Company> mydetailCompany();
}
