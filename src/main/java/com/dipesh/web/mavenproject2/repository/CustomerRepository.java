/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.web.mavenproject2.repository;

import com.dipesh.web.mavenproject2.entity.Customer;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Dipesh
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
   @Query(value = "select * from tbl_client where status=1", nativeQuery = true)
   List<Customer> myactiveCustomer();
   
   @Query(value = "select * from tbl_client where status= 0", nativeQuery = true)
   List<Customer> myinactiveCustomer();
   
   @Query(value = "select * from tbl_client where name=?", nativeQuery = true)
   List<Customer> mydetailCustomer();
}
