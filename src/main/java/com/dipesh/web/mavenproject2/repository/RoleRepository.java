/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.web.mavenproject2.repository;

import com.dipesh.web.mavenproject2.entity.Role;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Dipesh
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
}
